let data;

async function fetchData() {
  const response = await fetch("./data.json");
  data = await response.json();
}

function drawData() {
  if (document.querySelector("#mainTable tbody"))
    document.querySelector("#mainTable tbody").remove();
  let mainContainer = document.createElement("tbody");

  document.getElementById("mainTable").appendChild(mainContainer);

  for (let i = 0; i < data.length; i++) {
    let rowElement = document.createElement("tr");

    let nameElement = document.createElement("td");
    let surnameElement = document.createElement("td");
    let aboutElement = document.createElement("td");
    let eyecolorElement = document.createElement("td");

    nameElement.innerText = data[i].name.firstName;
    surnameElement.innerText = data[i].name.lastName;
    aboutElement.innerText = data[i].about;
    eyecolorElement.innerText = data[i].eyeColor;

    nameElement.classList.add("name");
    surnameElement.classList.add("surname");
    aboutElement.classList.add("about");
    eyecolorElement.classList.add("eyeColor");

    rowElement.appendChild(nameElement);
    rowElement.appendChild(surnameElement);
    rowElement.appendChild(aboutElement);
    rowElement.appendChild(eyecolorElement);

    rowElement.addEventListener("click", () => rowClicked(data[i]));
    rowElement.id = data[i].id;

    mainContainer.appendChild(rowElement);
  }
}

function rowClicked(row) {
  document.getElementById("nameChange").value = row.name.firstName;
  document.getElementById("surnameChange").value = row.name.lastName;
  document.getElementById("aboutChange").value = row.about;
  document.getElementById("eyecolorChange").value = row.eyeColor;
  document.getElementById("personID").value = row.id;
  document.getElementById("phone").value = row.phone;
}

function changeRow() {
  // event.preventDefault();
  // console.log(document.forms.changeDataForm);
  // let newData = Array.from(
  //   document.querySelectorAll("#changeDataForm input")
  // ).reduce((acc, input) => ({ ...acc, [input.id]: input.value }), {});

  // let newData = Array.from(
  //   document.querySelectorAll("#changeDataForm input")
  // ).map((el) => el.value);
  // document.getElementById(newData[5]).children.forEach((element, i) => {
  //   element.innerText = newData[i];
  // });

  // let newData = Array.from(
  //   document.querySelectorAll("#changeDataForm input")
  // ).reduce((acc, input) => ({ ...acc, [input.id]: input.value }), {});

  if (!document.getElementById("personID").value) return;

  //redraw an element
  let row = document.getElementById(document.getElementById("personID").value);
  row.querySelector(".name").innerText =
    document.querySelector("#nameChange").value;
  row.querySelector(".surname").innerText =
    document.querySelector("#surnameChange").value;
  row.querySelector(".about").innerText =
    document.querySelector("#aboutChange").value;
  row.querySelector(".eyeColor").innerText =
    document.querySelector("#eyecolorChange").value;

  //change the data array
  let index = data.findIndex(
    (el) => el.id == document.getElementById("personID").value
  );
  data[index].name.firstName = document.querySelector("#nameChange").value;
  data[index].name.lastName = document.querySelector("#surnameChange").value;
  data[index].about = document.querySelector("#aboutChange").value;
  data[index].eyeColor = document.querySelector("#eyecolorChange").value;

  //change JSON
}

function sortByColUp(field1, field2) {
  if (field2)
    data.sort((a, b) =>
      a[field1][field2] > b[field1][field2]
        ? 1
        : a[field1][field2] === b[field1][field2]
        ? a.id > b.id
          ? 1
          : -1
        : -1
    );
  else
    data.sort((a, b) =>
      a[field1] > b[field1]
        ? 1
        : a[field1] === b[field1]
        ? a.id > b.id
          ? 1
          : -1
        : -1
    );
  drawData();
}
function sortByColDown(field1, field2) {
  if (field2)
    data.sort((a, b) =>
      a[field1][field2] > b[field1][field2]
        ? -1
        : a[field1][field2] === b[field1][field2]
        ? a.id > b.id
          ? -1
          : 1
        : 1
    );
  else
    data.sort((a, b) =>
      a[field1] > b[field1]
        ? -1
        : a[field1] === b[field1]
        ? a.id > b.id
          ? -1
          : 1
        : 1
    );
  drawData();
}

async function main() {
  await fetchData();
  drawData();
}

main();
